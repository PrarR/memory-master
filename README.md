# Memory Master

The Memory Master computer game is a student project carried out while studying at the **Wrocław University of Science & Technology**. The aim of the project was to expand programming skills in Java, and thus to learn about the advantages, disadvantages and differences in relation to other programming languages, such as application speed, syntax simplicity, listening to events, etc. A computer game is a reference to the classics of the genre, i.e. for a memory game, where the player has to remember as many cards as possible. 

The game has three single player modes: 
- Standard 
- Shuffle 
- Time

The first one is a classic memory game and allows you to exercise your memory skills. The second mode is characterized by shuffling the cards every given number of moves, while the Time mode, as the name suggests, is about collecting cards on time as quickly as possible. In addition, it is possible to play multiplayer with both a friend and artificial intelligence. Correctly guessing a pair of cards is scored, the person with the most points wins. The available game modes for playing against a friend and the computer are Standard and Shuffle. 

# Team

- The content (idea, window arrangement, game modes, etc) and programming was done by **Game Developer Piotr Kupczyk**. 
- The entire project received a visual setting by **Graphic Designer Marta Radojewska**. 

All rights reserved © 2021
