import Controllers.AudioController;
import Windows.MainWindow;

import java.net.URL;

public class Main
{
    public static final URL MUSIC_PATH = Main.class.getResource("Soundtrack/MainSoundtrack.wav");

    public static void main(String[] args)
    {
        AudioController audioController = new AudioController();
        audioController.playMusic(MUSIC_PATH.getPath());

        javax.swing.SwingUtilities.invokeLater(() -> {
            MainWindow mainWindow = new MainWindow();
            mainWindow.showWindow();
        });
    }
}