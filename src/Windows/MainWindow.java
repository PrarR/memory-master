package Windows;

import javax.swing.*;
import Components.MenuComponent;

import java.awt.*;

public class MainWindow extends JFrame
{
    private MainWindow frame;
    private static double prescalerWidth;

    public MainWindow()
    {
        setTitle("Memory Master");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(screenSize.width, screenSize.height);

        setIconImage(new ImageIcon(getClass().getResource("../Graphics/Backgrounds/BackgroundMainMenu.jpg")).
                getImage().getScaledInstance(screenSize.width, screenSize.height, Image.SCALE_SMOOTH));

        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setUndecorated(true);
    }

    public void showWindow()
    {
        if (frame == null)
        {
            frame = new MainWindow();
            calculatePrescalers();
        }

        add(new MenuComponent());

        setResizable(false);
        setVisible(true);
    }

    private void calculatePrescalers()
    {
        double referenceWidth = 1536;
        prescalerWidth = frame.getBounds().width / referenceWidth;
    }

    public static double getPrescalerWidth()
    {
        return prescalerWidth;
    }
}
