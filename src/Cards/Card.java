package Cards;

import Controllers.CardsController;
import Managers.SettingsManager;
import Windows.MainWindow;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

public class Card extends JButton
{
    private final static String CARDS_PATH = "../Graphics/Cards/card";

    private final String id;
    private final ImageIcon cardFrontImageIcon;
    private boolean clicked;

    public Card(String id, CardsController cardsController, SettingsManager settingsManager)
    {
        this.id = id;

        int imageSize = 200 - settingsManager.getDifficulty().getIndex() * 28;
        if (settingsManager.getPlayerMode() == SettingsManager.PlayerMode.Singleplayer)
            imageSize *= MainWindow.getPrescalerWidth();

        Image image = new ImageIcon(getClass().getResource(CARDS_PATH + id + ".png"))
                .getImage().getScaledInstance((imageSize), imageSize, Image.SCALE_SMOOTH);

        cardFrontImageIcon = new ImageIcon(image);

        setBasicCardSettings(cardsController);

        Border raisedBevel = BorderFactory.createRaisedBevelBorder();
        Border loweredBevel = BorderFactory.createLoweredBevelBorder();
        setBorder(BorderFactory.createCompoundBorder(raisedBevel, loweredBevel));

        setCursor(new Cursor(Cursor.HAND_CURSOR));

        addActionListener((e) ->
        {
            setClickedState();
        });
    }

    public void setBasicCardSettings(CardsController cardsController)
    {
        setIcon(cardsController.getCardReverseImageIcon());
        setDisabledIcon(cardFrontImageIcon);
        setEnabled(true);
        clicked = false;
    }

    public String getId()
    {
        return id;
    }

    public ImageIcon getCardFrontImageIcon()
    {
        return cardFrontImageIcon;
    }

    public boolean isClicked()
    {
        return clicked;
    }

    public void setClickedState()
    {
        setIcon(cardFrontImageIcon);
        setEnabled(false);
        clicked = true;
    }
}
