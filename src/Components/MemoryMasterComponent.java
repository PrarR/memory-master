package Components;

import Buttons.GameButton;
import Labels.MenuLabel;
import Windows.MainWindow;

import javax.swing.*;
import java.awt.*;

public abstract class MemoryMasterComponent extends JComponent
{
    protected final static String BACKGROUND_GRAPHICS_PATH = "../Graphics/Backgrounds/BackgroundMainMenu.jpg";
    protected final static float MARGIN_LEFT_RIGHT_DIVIDER = 8 / 3f;
    protected final static float MARGIN_BOTTOM_TOP_DIVIDER = 4.5f;
    protected final static float VGAP_DIVIDER = 10f;

    protected JPanel panel;
    protected Image image;
    protected int borderBottomTop;
    protected int borderLeftRight;
    protected int screenWidth;
    protected int screenHeight;

    protected void calculateBorders()
    {
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        double width = screenSize.getWidth();
        double height = screenSize.getHeight();

        borderBottomTop = (int) (height / MARGIN_BOTTOM_TOP_DIVIDER);
        borderLeftRight = (int) (width / MARGIN_LEFT_RIGHT_DIVIDER);

        screenWidth = (int) width;
        screenHeight = (int) height;
    }

    protected void setIcon(String imagePath)
    {
        if (screenWidth <= 0 || screenHeight <= 0)
        {
            image = null;
            return;
        }

        Image icon = new ImageIcon(getClass().getResource(imagePath)).getImage().getScaledInstance(screenWidth, screenHeight, Image.SCALE_SMOOTH);
        image = new ImageIcon(icon).getImage();
    }

    protected void createGameTitleLabel()
    {
        MenuLabel gameTitleLabel = new MenuLabel("Memory Master", new Font(Font.SANS_SERIF, Font.BOLD,
                (int) (48 * MainWindow.getPrescalerWidth())), new Color(66, 40, 5));
        panel.add(gameTitleLabel);
    }

    protected void createReturnButton()
    {
        GameButton returnToMainMenu =
                new GameButton(
                        "Return to Main Menu",
                        new Color(184, 10, 10),
                        Font.SANS_SERIF,
                        Font.PLAIN,
                        (int) (38 * MainWindow.getPrescalerWidth()),
                        new Color(255, 255, 255)
                );

        returnToMainMenu.addActionListener(event ->
        {
            remove(panel);
            add(new MenuComponent());
            revalidate();
            repaint();
        });

        panel.add(returnToMainMenu);
    }

    @Override
    protected void paintComponent(Graphics graphics)
    {
        if (image == null)
            return;

        graphics.drawImage(image, 0, 0, null);
    }
}
