package Components;

import Labels.MenuLabel;
import Managers.SettingsManager.SecondPlayer;
import Windows.MainWindow;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class MultiPlayerGameResultComponent extends MemoryMasterComponent
{
    private final SecondPlayer secondPlayer;
    private final int firstPlayerPoints;
    private final int secondPlayerPoints;

    public MultiPlayerGameResultComponent(SecondPlayer secondPlayer, int firstPlayerPoints, int secondPlayerPoints)
    {
        this.firstPlayerPoints = firstPlayerPoints;
        this.secondPlayerPoints = secondPlayerPoints;
        this.secondPlayer = secondPlayer;

        setLayout(new BorderLayout());

        calculateBorders();
        setIcon(BACKGROUND_GRAPHICS_PATH);

        panel = new JPanel(new GridLayout(5, 1, 20, (int) (borderBottomTop / VGAP_DIVIDER )));
        panel.setBorder(new EmptyBorder(borderBottomTop, borderLeftRight, borderBottomTop, borderLeftRight));

        createGameResultLayout();

        panel.setOpaque(false);
    }

    private void createGameResultLayout()
    {
        createGameTitleLabel();

        if (firstPlayerPoints > secondPlayerPoints)
            createGameResultLabel("Congratulations! You won!");

        if (firstPlayerPoints == secondPlayerPoints)
            createGameResultLabel("Draw! What a fierce match!");

        if (firstPlayerPoints < secondPlayerPoints)
        {
            String text = "Congratulations! ";
            if (secondPlayer == SecondPlayer.AI)
                text = "You lose! ";
            createGameResultLabel(text + secondPlayer.toString() + " won!");
        }

        createReturnButton();

        add(panel, BorderLayout.CENTER);
    }

    private void createGameResultLabel(String labelText)
    {
        MenuLabel menuLabel = new MenuLabel(
                labelText,
                new Font(Font.SANS_SERIF, Font.PLAIN, (int) (28 * MainWindow.getPrescalerWidth())), Color.BLACK
        );
        panel.add(menuLabel);
    }
}
