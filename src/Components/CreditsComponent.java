
package Components;

import Buttons.GameButton;
import Labels.MenuLabel;
import Windows.MainWindow;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class CreditsComponent extends MemoryMasterComponent
{
    public CreditsComponent()
    {
        setLayout(new BorderLayout());

        calculateBorders();
        setIcon(BACKGROUND_GRAPHICS_PATH);

        panel = new JPanel(new GridLayout(5, 1, 20, (int) (borderBottomTop / VGAP_DIVIDER )));
        panel.setBorder(new EmptyBorder(borderBottomTop, borderLeftRight, borderBottomTop, borderLeftRight));

        createLayout();

        panel.setOpaque(false);
    }

    private void createLayout()
    {
        createGameTitleLabel();
        createCopyrightLabels();
        createReturnButton();

        add(panel, BorderLayout.CENTER);
    }

    private void createCopyrightLabels()
    {
        MenuLabel copyrightLabel = new MenuLabel(
                "Game Developer: Piotr Kupczyk",
                new Font(Font.SANS_SERIF, Font.PLAIN, (int) (24 * MainWindow.getPrescalerWidth())), Color.BLACK
        );
        panel.add(copyrightLabel);

        copyrightLabel = new MenuLabel(
                "Graphic Designer: Marta Radojewska",
                new Font(Font.SANS_SERIF, Font.PLAIN, (int) (23 * MainWindow.getPrescalerWidth())), Color.BLACK
        );
        panel.add(copyrightLabel);

        copyrightLabel = new MenuLabel(
                "All rights reserved © 2021",
                new Font(Font.SANS_SERIF, Font.PLAIN, (int) (24 * MainWindow.getPrescalerWidth())), Color.BLACK
        );
        panel.add(copyrightLabel);
    }
}
