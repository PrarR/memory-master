package Components;

import Controllers.CardsController;
import Controllers.GameController;
import Managers.PlayersManager;
import Managers.SettingsManager;
import Panels.CardsPanel;
import Panels.GamePanel;
import Panels.PlayersPanel;

import java.awt.*;

public class BoardComponent extends MemoryMasterComponent
{
    protected final static String GAME_BACKGROUND_GRAPHICS_PATH = "../Graphics/Backgrounds/BackgroundSky.png";

    public BoardComponent(SettingsManager settingsManager)
    {
        setLayout(new BorderLayout());

        calculateBorders();
        setIcon(GAME_BACKGROUND_GRAPHICS_PATH);

        CardsController cardsController = new CardsController(settingsManager);
        CardsPanel cardsPanel = new CardsPanel(settingsManager, cardsController);

        GameController gameController;
        if (settingsManager.getPlayerMode() == SettingsManager.PlayerMode.Multiplayer)
        {
            PlayersManager playersManager = new PlayersManager(settingsManager);
            PlayersPanel playersPanel = new PlayersPanel(settingsManager, playersManager);
            add(playersPanel, BorderLayout.NORTH);

            gameController = new GameController(settingsManager, cardsController, playersManager, this);
        }
        else
            gameController = new GameController(settingsManager, cardsController, this);

        GamePanel gamePanel = new GamePanel(gameController);

        add(cardsPanel, BorderLayout.CENTER);
        add(gamePanel, BorderLayout.SOUTH);

        cardsPanel.setOpaque(false);

        gameController.start();
    }
}
