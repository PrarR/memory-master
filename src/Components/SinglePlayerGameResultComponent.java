package Components;

import Labels.MenuLabel;
import Windows.MainWindow;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class SinglePlayerGameResultComponent extends MemoryMasterComponent
{
    private final boolean isWin;

    public SinglePlayerGameResultComponent(boolean isWin)
    {
        this.isWin = isWin;

        setLayout(new BorderLayout());

        calculateBorders();
        setIcon(BACKGROUND_GRAPHICS_PATH);

        panel = new JPanel(new GridLayout(5, 1, 20, (int) (borderBottomTop / VGAP_DIVIDER )));
        panel.setBorder(new EmptyBorder(borderBottomTop, borderLeftRight, borderBottomTop, borderLeftRight));

        createGameResultLayout();

        panel.setOpaque(false);
    }

    private void createGameResultLayout()
    {
        createGameTitleLabel();

        if (isWin)
            createWinGameLabel();
        else
            createLoseGameLabel();

        createReturnButton();

        add(panel, BorderLayout.CENTER);
    }

    private void createWinGameLabel()
    {
        MenuLabel winLabel = new MenuLabel(
                "Congratulations! You won!",
                new Font(Font.SANS_SERIF, Font.PLAIN, (int) (28 * MainWindow.getPrescalerWidth())), Color.BLACK
        );
        panel.add(winLabel);
    }

    private void createLoseGameLabel()
    {
        Font loseLabelFont = new Font(Font.SANS_SERIF, Font.PLAIN, (int) (28 * MainWindow.getPrescalerWidth()));

        MenuLabel loseLabel1 = new MenuLabel("Oh no! You have lost!", loseLabelFont, Color.BLACK);
        MenuLabel loseLabel2 = new MenuLabel("Better luck next time!", loseLabelFont, Color.BLACK);

        panel.add(loseLabel1);
        panel.add(loseLabel2);
    }
}
