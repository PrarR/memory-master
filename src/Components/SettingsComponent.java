
package Components;

import Managers.SettingsManager;
import Managers.SettingsManager.Difficulty;
import Managers.SettingsManager.GameMode;
import Managers.SettingsManager.PlayerMode;
import Managers.SettingsManager.SecondPlayer;
import Windows.MainWindow;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class SettingsComponent extends MemoryMasterComponent
{
    private final SettingsManager settingsManager;

    public SettingsComponent(SettingsManager settingsManager)
    {
        setLayout(new BorderLayout());

        this.settingsManager = settingsManager;

        calculateBorders();
        setIcon(BACKGROUND_GRAPHICS_PATH);

        panel = new JPanel(new GridLayout(6, 1, 20, (int) (borderBottomTop / VGAP_DIVIDER )));
        panel.setBorder(new EmptyBorder(borderBottomTop, borderLeftRight, borderBottomTop, borderLeftRight));

        createSettingsLayout();

        panel.setOpaque(false);
    }

    private void createSettingsLayout()
    {
        createGameTitleLabel();
        createComboBoxes();
        createReturnButton();

        add(panel, BorderLayout.CENTER);
    }

    private void createComboBoxes()
    {
        JComboBox<Difficulty> difficultyComboBox = new JComboBox<>(Difficulty.values());
        JComboBox<PlayerMode> playerModeComboBox = new JComboBox<>(PlayerMode.values());
        JComboBox<SecondPlayer> secondPlayerComboBox = new JComboBox<>(SecondPlayer.values());
        JComboBox<GameMode> gameModeComboBox;

        boolean isMultiplayer = settingsManager.getPlayerMode() == PlayerMode.Multiplayer;
        if (isMultiplayer)
        {
            GameMode[] values = GameMode.values();
            GameMode[] newValues = new GameMode[values.length - 1];

            int index = 0;
            for (int i = 0; i < GameMode.values().length; ++i)
            {
                GameMode value = values[i];
                if (value != GameMode.Time)
                    newValues[index++] = value;
            }

            gameModeComboBox = new JComboBox<>(newValues);
        }
        else
            gameModeComboBox = new JComboBox<>(GameMode.values());

        createComboBox(difficultyComboBox);
        createComboBox(playerModeComboBox);

        if (isMultiplayer)
            createComboBox(secondPlayerComboBox);

        createComboBox(gameModeComboBox);

        difficultyComboBox.setSelectedItem(settingsManager.getDifficulty());
        playerModeComboBox.setSelectedItem(settingsManager.getPlayerMode());
        secondPlayerComboBox.setSelectedItem(settingsManager.getSecondPlayer());
        gameModeComboBox.setSelectedItem(settingsManager.getGameMode());

        difficultyComboBox.addActionListener(event ->
        {
            String text = String.valueOf(difficultyComboBox.getSelectedItem());
            Difficulty difficulty = Difficulty.valueOf(text);
            settingsManager.setDifficulty(difficulty);
        });

        playerModeComboBox.addActionListener(event ->
        {
            String text = String.valueOf(playerModeComboBox.getSelectedItem());
            PlayerMode playerMode = PlayerMode.valueOf(text);
            settingsManager.setPlayerMode(playerMode);

            remove(panel);
            add(new SettingsComponent(settingsManager));
            revalidate();
            repaint();
        });

        secondPlayerComboBox.addActionListener(event ->
        {
            String text = String.valueOf(secondPlayerComboBox.getSelectedItem());
            SecondPlayer secondPlayer = SecondPlayer.valueOf(text);
            settingsManager.setSecondPlayer(secondPlayer);
        });

        gameModeComboBox.addActionListener(event ->
        {
            String text = String.valueOf(gameModeComboBox.getSelectedItem());
            GameMode mode = GameMode.valueOf(text);
            settingsManager.setGameMode(mode);
        });
    }

    private void createComboBox(JComboBox<?> comboBox)
    {
        comboBox.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, (int) (42 * MainWindow.getPrescalerWidth())));
        comboBox.setForeground(new Color(255, 255, 255));
        comboBox.setBackground(new Color(40, 51, 122));

        panel.add(comboBox);
    }
}
