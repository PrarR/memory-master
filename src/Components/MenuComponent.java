package Components;

import Buttons.GameButton;
import Managers.SettingsManager;
import Windows.MainWindow;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class MenuComponent extends MemoryMasterComponent
{
    private static SettingsManager settingsManager;

    public MenuComponent()
    {
        if (settingsManager == null)
            settingsManager = new SettingsManager();

        SetMenuComponent();
    }

    private void SetMenuComponent()
    {
        setLayout(new BorderLayout());

        calculateBorders();
        setIcon(BACKGROUND_GRAPHICS_PATH);

        panel = new JPanel(new GridLayout(6, 1, 20, (int) (borderBottomTop / VGAP_DIVIDER )));
        panel.setBorder(new EmptyBorder(borderBottomTop, borderLeftRight, borderBottomTop / 2, borderLeftRight));

        createMenuLayout();

        panel.setOpaque(false);
    }

    private void createMenuLayout()
    {
        createGameTitleLabel();
        createStartGameButton();
        createSettingsButton();
        createCopyrightButton();
        createReturnButton();

        add(panel, BorderLayout.CENTER);
    }

    private void createStartGameButton()
    {
        GameButton startGameButton =
                new GameButton(
                        "Start Game",
                        new Color(40, 51, 122),
                        Font.SANS_SERIF,
                        Font.PLAIN,
                        (int) (40 * MainWindow.getPrescalerWidth()),
                        new Color(255, 255, 255)
                );

        startGameButton.addActionListener(event ->
        {
            remove(panel);
            add(new BoardComponent(settingsManager));
            revalidate();
            repaint();
        });

        panel.add(startGameButton);
    }

    private void createSettingsButton()
    {
        GameButton settingsButton =
                new GameButton(
                        "Modes & Settings",
                        new Color(40, 51, 122),
                        Font.SANS_SERIF,
                        Font.PLAIN,
                        (int) (40 * MainWindow.getPrescalerWidth()),
                        new Color(255, 255, 255)
                );

        settingsButton.addActionListener(event ->
        {
            remove(panel);
            add(new SettingsComponent(settingsManager));
            revalidate();
            repaint();
        });

        panel.add(settingsButton);
    }

    private void createCopyrightButton()
    {
        GameButton creditsButton =
                new GameButton(
                        "Credits",
                        new Color(40, 51, 122),
                        Font.SANS_SERIF,
                        Font.PLAIN,
                        (int) (40 * MainWindow.getPrescalerWidth()),
                        new Color(255, 255, 255)
                );

        creditsButton.addActionListener(event ->
        {
            remove(panel);
            add(new CreditsComponent());
            revalidate();
            repaint();
        });

        panel.add(creditsButton);
    }

    @Override
    protected void createReturnButton()
    {
        GameButton closeButton =
                new GameButton(
                        "Close",
                        new Color(184, 10, 10),
                        Font.SANS_SERIF,
                        Font.PLAIN,
                        (int) (40 * MainWindow.getPrescalerWidth()),
                        new Color(255, 255, 255)
                );

        closeButton.addActionListener(e -> System.exit(0));
        panel.add(closeButton);
    }
}
