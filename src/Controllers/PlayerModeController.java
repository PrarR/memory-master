package Controllers;

import Cards.Card;
import Components.BoardComponent;
import Managers.SettingsManager;
import Panels.CardsPanel;

import java.util.List;
import java.util.Random;

public abstract class PlayerModeController
{
    protected static final String REMAINING_MOVES_TO_SHUFFLE = "Remaining moves to shuffle: ";

    protected final GameController gameController;
    protected final CardsController cardsController;
    protected final BoardComponent boardComponent;
    protected final SettingsManager.Difficulty difficulty;
    protected final SettingsManager.SecondPlayer secondPlayer;
    protected final SettingsManager.GameMode mode;
    protected final Random random;
    protected final List<Card> cards;

    protected Card firstCard;
    protected Card secondCard;
    protected int numberOfSelectedCards;
    protected int remainingMovesToShuffle;
    protected int goodPairs;
    protected boolean isGame;

    public PlayerModeController(GameController gameController)
    {
        this.gameController = gameController;
        cardsController = gameController.getCardsController();
        boardComponent = gameController.getBoardComponent();

        SettingsManager settingsManager = gameController.getSettingsManager();
        difficulty = settingsManager.getDifficulty();
        secondPlayer = settingsManager.getSecondPlayer();
        mode = settingsManager.getGameMode();
        random = settingsManager.getRandom();

        cards = cardsController.getCards();

        remainingMovesToShuffle = difficulty.getNumberOfMovesToShuffle();
        goodPairs = 0;
        isGame = true;
    }

    protected void selectCards()
    {
        firstCard = null;
        secondCard = null;
        numberOfSelectedCards = 0;

        for (int index = 0; index < cards.size() && numberOfSelectedCards < 2; ++index)
        {
            Card card = cards.get(index);
            if (card.isClicked())
            {
                if (firstCard == null)
                    firstCard = card;
                else
                    secondCard = card;

                ++numberOfSelectedCards;
            }
        }
    }

    protected void checkCards()
    {
        if (numberOfSelectedCards == 2)
        {
            if (mode == SettingsManager.GameMode.Shuffle)
            {
                --remainingMovesToShuffle;
                gameController.getGamePanel().setLabelText(REMAINING_MOVES_TO_SHUFFLE + remainingMovesToShuffle);
            }

            lockCardsPanel();
        }
    }

    protected void lockCardsPanel()
    {
        disableCardsPanel();

        firstCard.setDisabledIcon(firstCard.getCardFrontImageIcon());
        secondCard.setDisabledIcon(secondCard.getCardFrontImageIcon());

        unlockCardsPanelAfterTime();
    }

    protected void disableCardsPanel()
    {
        for (Card card : cards)
        {
            card.setEnabled(false);
            card.setDisabledIcon(cardsController.getCardReverseImageIcon());
        }
    }

    protected void unlockCardsPanelAfterTime()
    {
        try {
            Thread.sleep(1500);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        while (gameController.getGamePanel().isPause())
        {
            try {
                Thread.sleep(100);
            } catch (InterruptedException exception) {
                exception.printStackTrace();
            }
        }

        if (firstCard.getId().equals(secondCard.getId()))
        {
            ++goodPairs;
            firstCard.setVisible(false);
            secondCard.setVisible(false);
        }

        for (Card card : cards)
            card.setBasicCardSettings(cardsController);
    }

    protected void shuffleCards()
    {
        if (remainingMovesToShuffle == 0)
        {
            CardsPanel cardsPanel = cardsController.getCardsPanel();
            cardsPanel.generateRandomCards(cards);

            boardComponent.revalidate();
            boardComponent.repaint();

            int visibleCards = 0;
            for (Card Card: cards)
            {
                if (Card.isVisible())
                    ++visibleCards;
            }

            double movesPrescaler = (double) visibleCards / cards.size();

            if (movesPrescaler >= 0.3f)
                remainingMovesToShuffle = (int) Math.ceil((difficulty.getNumberOfMovesToShuffle() * movesPrescaler));
            else
                remainingMovesToShuffle = 2 + difficulty.getIndex();

            gameController.getGamePanel().setLabelText(REMAINING_MOVES_TO_SHUFFLE + remainingMovesToShuffle);
        }
    }
}
