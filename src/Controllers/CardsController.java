package Controllers;

import Cards.Card;
import Managers.SettingsManager;
import Panels.CardsPanel;
import Windows.MainWindow;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CardsController
{
    public final static int PAIR = 2;
    private final static String CARD_REVERSE_PATH = "../Graphics/Cards/cardReverse.png";

    private final SettingsManager settingsManager;
    private final ImageIcon cardReverseImageIcon;
    private final List<Card> cards;

    private CardsPanel cardsPanel;

    public CardsController(SettingsManager settingsManager)
    {
        this.settingsManager = settingsManager;

        cards = new ArrayList<>();

        int imageSize = 200 - settingsManager.getDifficulty().getIndex() * 25;
        if (settingsManager.getPlayerMode() == SettingsManager.PlayerMode.Singleplayer)
            imageSize *= MainWindow.getPrescalerWidth();

        Image image = new ImageIcon(getClass().getResource(CARD_REVERSE_PATH))
                .getImage().getScaledInstance(imageSize, imageSize, Image.SCALE_SMOOTH);
        cardReverseImageIcon = new ImageIcon(image);

        for (int index = 0; index < settingsManager.getDifficulty().getNumberOfCards() / PAIR; ++index)
        {
            for (int i = 0; i < PAIR; ++i)
            {
                Card card = new Card(String.valueOf(index), this, settingsManager);
                cards.add(card);
            }
        }
    }

    public void setCardsPanel(CardsPanel cardsPanel)
    {
        this.cardsPanel = cardsPanel;
    }

    public CardsPanel getCardsPanel()
    {
        return Objects.requireNonNullElseGet(cardsPanel, () -> new CardsPanel(settingsManager, this));
    }

    public List<Card> getCards()
    {
        return cards;
    }

    public ImageIcon getCardReverseImageIcon()
    {
        return cardReverseImageIcon;
    }
}
