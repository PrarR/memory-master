package Controllers;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import java.io.File;

public class AudioController
{
    public void playMusic(String filepath)
    {
        try {
            File musicPath = new File(filepath);

            if (musicPath.exists())
            {
                AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(musicPath);
                Clip clip = AudioSystem.getClip();
                clip.open(audioInputStream);
                setVolume(clip);
                clip.start();
                clip.loop(Clip.LOOP_CONTINUOUSLY);
            }
            else
                System.out.println("Can't find file!");
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    private void setVolume(Clip clip)
    {
        FloatControl gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
        double gain = 0.18;
        float dB = (float) (Math.log(gain) / Math.log(10.0) * 20.0);
        gainControl.setValue(dB);
    }
}
