package Controllers;

import Cards.Card;
import Components.MultiPlayerGameResultComponent;
import Managers.PlayersManager;
import Managers.PlayersManager.Turn;
import Managers.SettingsManager;
import Managers.SettingsManager.SecondPlayer;

import java.util.ArrayList;
import java.util.List;

public class MultiPlayerController extends PlayerModeController
{
    private final PlayersManager playersManager;
    private List<Card> memoryAI;

    public MultiPlayerController(GameController gameController)
    {
        super(gameController);
        playersManager = gameController.getPlayersManager();
        memoryAI = new ArrayList<>();
    }

    public void multiplayerStandard()
    {
        while (isGame)
        {
            selectCards();
            checkCards();
            checkGameState();
        }
    }

    public void multiplayerShuffle()
    {
        gameController.getGamePanel().setLabelText(REMAINING_MOVES_TO_SHUFFLE + remainingMovesToShuffle);

        while (isGame)
        {
            selectCards();
            checkCards();
            if (remainingMovesToShuffle == 0 && secondPlayer == SecondPlayer.AI)
                memoryAI = new ArrayList<>();
            shuffleCards();
            checkGameState();
        }
    }

    @Override
    protected void selectCards()
    {
        if (playersManager.getPlayerTurn() == Turn.SecondPlayer && secondPlayer == SecondPlayer.AI)
        {
            disableCardsPanel();
            selectCardsByAI();
            return;
        }

        super.selectCards();
    }

    @Override
    protected void checkCards()
    {
        int pairs = goodPairs;

        super.checkCards();

        if (numberOfSelectedCards == 2)
        {
            resizeMemoryAI();

            if (goodPairs - pairs == 1)
            {
                while (memoryAI.contains(firstCard))
                    memoryAI.remove(firstCard);

                while (memoryAI.contains(secondCard))
                    memoryAI.remove(secondCard);

                playersManager.addPlayersPoints();
            }

            playersManager.setNextTurn();
        }
    }

    private void resizeMemoryAI()
    {
        if (playersManager.getPlayerTurn() == Turn.FirstPlayer && secondPlayer == SecondPlayer.AI)
        {
            memoryAI.add(firstCard);
            memoryAI.add(secondCard);
        }

        while (memoryAI.size() > difficulty.getNumberOfRememberedMoves() * CardsController.PAIR)
        {
            memoryAI.remove(0);
        }
    }

    @Override
    protected void lockCardsPanel()
    {
        disableCardsPanel();

        firstCard.setDisabledIcon(firstCard.getCardFrontImageIcon());

        if (secondPlayer == SettingsManager.SecondPlayer.AI && playersManager.getPlayerTurn() == Turn.SecondPlayer )
        {
            try {
                Thread.sleep(random.nextInt(700) + 500);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        secondCard.setDisabledIcon(secondCard.getCardFrontImageIcon());

        unlockCardsPanelAfterTime();
    }

    private void checkGameState()
    {
        if (goodPairs == difficulty.getNumberOfCards() / 2)
        {
            showGameResult();
            isGame = false;
        }
    }

    private void showGameResult()
    {
        boardComponent.removeAll();
        boardComponent.add(new MultiPlayerGameResultComponent(
                secondPlayer,
                playersManager.getFirstPlayerPoints(),
                playersManager.getSecondPlayerPoints())
        );
        boardComponent.revalidate();
        boardComponent.repaint();
    }

    private void selectCardsByAI()
    {
        try {
            Thread.sleep(random.nextInt(1650) + 200);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        firstCard = null;
        secondCard = null;
        numberOfSelectedCards = 0;

        List<Integer> cardsIndexes = new ArrayList<>();

        for (int index = 0; index < cards.size(); ++index)
        {
            Card card = cards.get(index);
            if (card.isVisible() && !card.isClicked())
                cardsIndexes.add(index);
        }

        for (int index = 0; index < cards.size() && numberOfSelectedCards < 2; ++index)
        {
            Card[] pair = checkMemoryAI();

            if (pair == null)
            {
                int randCardIndex = random.nextInt(cardsIndexes.size());
                Card card = cards.get(cardsIndexes.get(randCardIndex));
                card.setClickedState();
                cardsIndexes.remove(randCardIndex);

                if (firstCard == null)
                {
                    firstCard = card;
                    if (numberOfSelectedCards == 0)
                        memoryAI.add(firstCard);
                }
                else
                {
                    secondCard = card;
                    memoryAI.add(secondCard);
                }

                ++numberOfSelectedCards;
            }
            else
            {
                firstCard = pair[0];
                firstCard.setClickedState();
                memoryAI.add(firstCard);

                secondCard = pair[1];
                secondCard.setClickedState();
                memoryAI.add(secondCard);

                numberOfSelectedCards += 2;
            }
        }
    }

    private Card[] checkMemoryAI()
    {
        for (int i = 0; i < memoryAI.size(); ++i)
        {
            for (int j = 0; j < memoryAI.size(); ++j)
            {
                if (i == j)
                    continue;

                Card elementI = memoryAI.get(i);
                Card elementJ = memoryAI.get(j);

                if (elementI.getId().equals(elementJ.getId()) && elementI != elementJ)
                {
                    int percentageChance = random.nextInt(100);

                    if (percentageChance < difficulty.getChanceForCorrectMatch())
                        return new Card[]{elementI, elementJ};
                    else
                        return null;
                }
            }
        }

        return null;
    }
}
