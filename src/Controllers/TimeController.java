package Controllers;

import Managers.SettingsManager.Difficulty;
import Panels.GamePanel;

import java.util.concurrent.TimeUnit;

public class TimeController extends Thread
{
    private final static String TIME_TEXT = "Time: ";
    private final GameController gameController;
    private final GamePanel gamePanel;
    private int currentTime;

    public TimeController(GameController gameController, Difficulty difficulty)
    {
        this.gameController = gameController;
        gamePanel = gameController.getGamePanel();
        currentTime = difficulty.getTime();
        gamePanel.setLabelText(TIME_TEXT + currentTime);
    }

    public void run()
    {
        int millis = currentTime * 1000;

        String hms = String.format("%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(millis) % TimeUnit.HOURS.toMinutes(1),
                TimeUnit.MILLISECONDS.toSeconds(millis) % TimeUnit.MINUTES.toSeconds(1)
        );
        gamePanel.setLabelText(TIME_TEXT + hms);

        while (true)
        {
            if (gamePanel.isPause())
            {
                try {
                    Thread.sleep(33);
                } catch (InterruptedException exception) {
                    exception.printStackTrace();
                }
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException exception) {
                exception.printStackTrace();
            }

            --currentTime;
            if (currentTime <= 0)
            {
                gameController.setTimeUp(true);
                break;
            }

            if (gamePanel.isPause())
                currentTime += 1;

            millis = currentTime * 1000;

            hms = String.format("%02d:%02d",
                    TimeUnit.MILLISECONDS.toMinutes(millis) % TimeUnit.HOURS.toMinutes(1),
                    TimeUnit.MILLISECONDS.toSeconds(millis) % TimeUnit.MINUTES.toSeconds(1)
            );
            gamePanel.setLabelText(TIME_TEXT + hms);
        }
    }
}
