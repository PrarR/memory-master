package Controllers;

import Components.SinglePlayerGameResultComponent;
import Managers.SettingsManager;

public class SinglePlayerController extends PlayerModeController
{
    private static final String REMAINING_MOVES_TO_SHUFFLE = "Remaining moves to shuffle: ";
    private Thread timeController;

    public SinglePlayerController(GameController gameController)
    {
        super(gameController);
    }

    public void singlePlayerStandard()
    {
        while (isGame)
        {
            selectCards();
            checkCards();
            checkGameState();
        }
    }

    public void singlePlayerShuffle()
    {
        gameController.getGamePanel().setLabelText(REMAINING_MOVES_TO_SHUFFLE + remainingMovesToShuffle);

        while (isGame)
        {
            selectCards();
            checkCards();
            shuffleCards();
            checkGameState();
        }
    }

    public void singlePlayerTime()
    {
        gameController.setTimeUp(false);

        timeController = new Thread(new TimeController(gameController, difficulty));
        timeController.start();

        while (isGame)
        {
            selectCards();
            checkCards();
            checkGameState();
        }
    }

    private void checkGameState()
    {
        if (goodPairs == difficulty.getNumberOfCards() / 2)
        {
            if (mode == SettingsManager.GameMode.Time)
                timeController.interrupt();

            showGameResult(true);
            isGame = false;
        }
        else
        {
            if (mode == SettingsManager.GameMode.Time && gameController.isTimeUp())
            {
                showGameResult(false);
                isGame = false;
            }
        }
    }

    private void showGameResult(boolean isWin)
    {
        boardComponent.removeAll();
        boardComponent.add(new SinglePlayerGameResultComponent(isWin));
        boardComponent.revalidate();
        boardComponent.repaint();
    }
}
