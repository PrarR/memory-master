package Controllers;

import Components.BoardComponent;
import Managers.PlayersManager;
import Managers.SettingsManager;
import Managers.SettingsManager.SecondPlayer;
import Managers.SettingsManager.PlayerMode;
import Managers.SettingsManager.GameMode;
import Panels.GamePanel;

import java.util.Objects;

public class GameController extends Thread
{
    private final SettingsManager settingsManager;
    private final CardsController cardsController;
    private final PlayersManager playersManager;
    private final BoardComponent boardComponent;
    private final PlayerMode playerMode;
    private final SecondPlayer secondPlayer;
    private final GameMode gameMode;

    private GamePanel gamePanel;
    private boolean timeUp;

    public GameController(SettingsManager settingsManager, CardsController cardsController, BoardComponent boardComponent)
    {
        this.settingsManager = settingsManager;
        this.cardsController = cardsController;
        playersManager = null;
        this.boardComponent = boardComponent;

        playerMode = settingsManager.getPlayerMode();
        secondPlayer = settingsManager.getSecondPlayer();
        gameMode = settingsManager.getGameMode();
    }

    public GameController(SettingsManager settingsManager, CardsController cardsController, PlayersManager playersManager, BoardComponent boardComponent)
    {
        this.settingsManager = settingsManager;
        this.cardsController = cardsController;
        this.playersManager = playersManager;
        this.boardComponent = boardComponent;

        playerMode = settingsManager.getPlayerMode();
        secondPlayer = settingsManager.getSecondPlayer();
        gameMode = settingsManager.getGameMode();
    }

    public void run()
    {
        SinglePlayerController singlePlayerController = new SinglePlayerController(this);
        MultiPlayerController multiPlayerController = new MultiPlayerController(this);

        switch (playerMode)
        {
            case Singleplayer ->
            {
                switch (gameMode)
                {
                    case Standard -> singlePlayerController.singlePlayerStandard();
                    case Shuffle -> singlePlayerController.singlePlayerShuffle();
                    case Time -> singlePlayerController.singlePlayerTime();
                }
            }
            case Multiplayer ->
            {
                switch (secondPlayer)
                {
                    case Friend, AI ->
                    {
                        switch (gameMode)
                        {
                            case Standard -> multiPlayerController.multiplayerStandard();
                            case Shuffle -> multiPlayerController.multiplayerShuffle();
                        }
                    }
                }
            }
        }
    }

    public void setGamePanel(GamePanel gamePanel)
    {
        this.gamePanel = gamePanel;
    }

    public GamePanel getGamePanel()
    {
        return Objects.requireNonNullElseGet(gamePanel, () -> new GamePanel(this));
    }

    public void setTimeUp(boolean timeUp)
    {
        this.timeUp = timeUp;
    }

    public boolean isTimeUp()
    {
        return timeUp;
    }

    public SettingsManager getSettingsManager()
    {
        return settingsManager;
    }

    public CardsController getCardsController()
    {
        return cardsController;
    }

    public PlayersManager getPlayersManager()
    {
        return Objects.requireNonNullElseGet(playersManager, () -> new PlayersManager(settingsManager));
    }

    public BoardComponent getBoardComponent()
    {
        return boardComponent;
    }
}


