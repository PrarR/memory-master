package Buttons;

import javax.swing.*;
import java.awt.*;

public class GameButton extends JButton
{
    public GameButton(String name, Color backgroundColor, String fontType, int fontStyle, int fontSize, Color fontColor)
    {
        super(name);
        setBackground(backgroundColor);

        setFont(new Font(fontType, fontStyle, fontSize));
        setForeground(fontColor);

        setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createRaisedBevelBorder(),
                BorderFactory.createLoweredBevelBorder()
        ));

        setCursor(new Cursor(Cursor.HAND_CURSOR));
    }
}