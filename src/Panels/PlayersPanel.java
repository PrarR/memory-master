package Panels;

import Components.MemoryMasterComponent;
import Managers.PlayersManager;
import Managers.PlayersManager.Turn;
import Labels.MenuLabel;
import Managers.SettingsManager;
import Managers.SettingsManager.SecondPlayer;
import Windows.MainWindow;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class PlayersPanel extends MemoryMasterComponent
{
    private final PlayersManager playersManager;
    private final SecondPlayer secondPlayer;
    private final JPanel panel;

    private MenuLabel currentTurnLabel;
    private MenuLabel firstPlayerPointsLabel;
    private MenuLabel secondPlayerPointsLabel;

    public PlayersPanel(SettingsManager settingsManager, PlayersManager playersManager)
    {
        this.secondPlayer = settingsManager.getSecondPlayer();
        this.playersManager = playersManager;

        setLayout(new BorderLayout());
        setOpaque(false);

        calculateBorders();
        panel = new JPanel(new GridLayout(2, 3, 0, 0));
        panel.setBorder(new EmptyBorder(30, 0, 0, 0));

        playersManager.setPlayersPanel(this);
        createLayout();

        panel.setOpaque(false);
    }

    private void createLayout()
    {
        createPlayerLabel("Player: You" , BorderLayout.LINE_START);
        createTurnLabel();
        createPlayerLabel("Player: " + secondPlayer.toString(), BorderLayout.LINE_END);
        createPlayerPointsLabel(Turn.FirstPlayer);
        createCurrentTurnLabel(getCurrentTurnText());
        createPlayerPointsLabel(Turn.SecondPlayer);

        add(panel);
    }

    private String getCurrentTurnText()
    {
        if (playersManager.getPlayerTurn() == Turn.FirstPlayer)
            return "You";
        else
            return secondPlayer.toString();
    }

    private void createPlayerLabel(String labelText, String borderLayoutLine)
    {
        MenuLabel playerLabel = new MenuLabel(labelText, new Font(Font.SANS_SERIF, Font.BOLD,
                (int) (32 * MainWindow.getPrescalerWidth())), new Color(255, 255, 255));
        panel.add(playerLabel, borderLayoutLine);
    }

    private void createPlayerPointsLabel(Turn turn)
    {
        if (turn == Turn.FirstPlayer)
        {
            firstPlayerPointsLabel = new MenuLabel("Points: 0", new Font(Font.SANS_SERIF, Font.BOLD,
                    (int) (32 * MainWindow.getPrescalerWidth())), new Color(255, 255, 255));

            panel.add(firstPlayerPointsLabel, BorderLayout.LINE_END);
            return;
        }

        if (turn == Turn.SecondPlayer)
        {
            secondPlayerPointsLabel = new MenuLabel("Points: 0", new Font(Font.SANS_SERIF, Font.BOLD,
                    (int) (32 * MainWindow.getPrescalerWidth())), new Color(255, 255, 255));

            panel.add(secondPlayerPointsLabel, BorderLayout.LINE_END);
        }
    }

    private void createTurnLabel()
    {
        MenuLabel turnLabel = new MenuLabel("Turn", new Font(Font.SANS_SERIF, Font.BOLD,
                (int) (32 * MainWindow.getPrescalerWidth())), new Color(195, 195, 255));

        panel.add(turnLabel);
    }

    private void createCurrentTurnLabel(String currentTurnText)
    {
        currentTurnLabel = new MenuLabel(currentTurnText, new Font(Font.SANS_SERIF, Font.BOLD,
                (int) (32 * MainWindow.getPrescalerWidth())), new Color(145, 145, 255));

        panel.add(currentTurnLabel);
    }

    public void updateCurrentTurnLabel(Turn turn, SecondPlayer secondPlayer)
    {
        if (turn == Turn.FirstPlayer)
        {
            currentTurnLabel.setText("You");
            return;
        }

        if (turn == Turn.SecondPlayer)
            currentTurnLabel.setText(secondPlayer.toString());
    }

    public void updatePlayerPointsLabel(Turn turn, int points)
    {
        if (turn == Turn.FirstPlayer)
        {
            firstPlayerPointsLabel.setText("Points: " + points);
            return;
        }

        if (turn == Turn.SecondPlayer)
            secondPlayerPointsLabel.setText("Points: " + points);
    }
}
