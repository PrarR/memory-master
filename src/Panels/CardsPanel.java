package Panels;

import Cards.Card;
import Controllers.CardsController;
import Managers.SettingsManager;
import Managers.SettingsManager.Difficulty;
import Managers.SettingsManager.PlayerMode;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.Collections;
import java.util.List;

public class CardsPanel extends JPanel
{
    private final static float BORDER_HEIGHT_PRESCALER = 18f;
    private final static float BORDER_WIDTH_SINGLEPLAYER_PRESCAlER = 4f;
    private final static float BORDER_WIDTH_MULTIPLAYER_PRESCALER = 3.6f;
    private final static float DIFFICULTY_BOARD_WIDTH_SINGLEPLAYER_PRESCALER = 3f;
    private final static float DIFFICULTY_BOARD_WIDTH_MULTIPLAYER_PRESCALER = 1.111f;

    private final Difficulty difficulty;
    private final PlayerMode mode;

    public CardsPanel(SettingsManager settingsManager, CardsController cardsController)
    {
        difficulty = settingsManager.getDifficulty();
        mode = settingsManager.getPlayerMode();

        cardsController.setCardsPanel(this);

        setCardsLayout();
        generateRandomCards(cardsController.getCards());
    }

    private void setCardsLayout()
    {
        setLayout(new GridLayout(
                difficulty.getNumberOfRows(),
                difficulty.getNumberOfColumns(),
                10, 10
        ));

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        double width = screenSize.getWidth();
        double height = screenSize.getHeight();

        int borderBotTop = (int) (1 / BORDER_HEIGHT_PRESCALER * height);

        int borderLeftRight;

        if (mode == PlayerMode.Singleplayer)
            borderLeftRight = (int) (1 / (BORDER_WIDTH_SINGLEPLAYER_PRESCAlER + DIFFICULTY_BOARD_WIDTH_SINGLEPLAYER_PRESCALER * difficulty.getIndex()) * width);
        else
            borderLeftRight = (int) (1 / (BORDER_WIDTH_MULTIPLAYER_PRESCALER + DIFFICULTY_BOARD_WIDTH_MULTIPLAYER_PRESCALER * difficulty.getIndex()) * width);
        setBorder(new EmptyBorder(borderBotTop, borderLeftRight, borderBotTop, borderLeftRight));
    }

    public void generateRandomCards(List<Card> cards)
    {
        Collections.shuffle(cards);

        removeAll();
        for (Card card: cards)
        {
            add(card);
        }
    }
}
