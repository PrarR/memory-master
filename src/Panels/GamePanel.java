package Panels;

import Cards.Card;
import Components.BoardComponent;
import Components.MenuComponent;
import Controllers.CardsController;
import Controllers.GameController;
import Managers.SettingsManager;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.List;

public class GamePanel extends JPanel
{
    private final static String BUTTONS_PATH = "../Graphics/Buttons/";
    private final static String PLAY_BUTTON_PATH = BUTTONS_PATH + "PlayButton.png";
    private final static String PAUSE_BUTTON_PATH = BUTTONS_PATH + "PauseButton.png";
    private final static String EXIT_BUTTON_PATH = BUTTONS_PATH + "ExitButton.png";

    private final JButton playPauseButton;
    private final ImageIcon playIcon;
    private final ImageIcon pauseIcon;
    private JLabel label;

    private final List<Card> cards;
    private boolean pause;

    public GamePanel(GameController gameController)
    {
        this.cards = gameController.getCardsController().getCards();

        gameController.setGamePanel(this);

        setLayout(new BorderLayout());
        setOpaque(false);

        playPauseButton = new JButton();
        playIcon = new ImageIcon(getClass().getResource(PLAY_BUTTON_PATH));
        pauseIcon = new ImageIcon(getClass().getResource(PAUSE_BUTTON_PATH));
        pause = false;

        createLayout(gameController);
    }

    private void createLayout(GameController gameController)
    {
        SettingsManager settingsManager = gameController.getSettingsManager();
        CardsController cardsController = gameController.getCardsController();
        BoardComponent boardComponent = gameController.getBoardComponent();

        if (settingsManager.getGameMode() == SettingsManager.GameMode.Shuffle
                || settingsManager.getGameMode() == SettingsManager.GameMode.Time)
            createLabel();

        JButton exitButton = new JButton();

        createButton(playPauseButton, PAUSE_BUTTON_PATH, (event) ->
        {
            pause = !pause;
            if (pause)
            {
                playPauseButton.setIcon(playIcon);

                for (Card card : cards)
                {
                    card.setEnabled(false);
                    card.setDisabledIcon(card.getIcon());
                }
            }
            else
            {
                playPauseButton.setIcon(pauseIcon);

                for (Card card : cards)
                    if (!card.isClicked())
                        card.setBasicCardSettings(cardsController);
            }
        });

        createButton(exitButton, EXIT_BUTTON_PATH, (event) ->
        {
            gameController.interrupt();

            Container parent = boardComponent.getParent();

            parent.remove(boardComponent);
            parent.add(new MenuComponent());
            parent.revalidate();
            parent.repaint();
        });

        JPanel buttonsPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 0));
        buttonsPanel.setOpaque(false);
        buttonsPanel.add(playPauseButton);
        buttonsPanel.add(exitButton);

        add(buttonsPanel, BorderLayout.LINE_END);
    }

    private void createLabel()
    {
        label = new JLabel();
        label.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 42));
        label.setForeground(Color.WHITE);
        label.setOpaque(false);
        label.setBorder(new EmptyBorder(0, 140, 0, 0));
        add(label, BorderLayout.CENTER);
    }

    private void createButton(JButton button, String buttonPath, ActionListener listener)
    {
        button.setIcon(new ImageIcon(getClass().getResource(buttonPath)));

        button.setBackground(Color.WHITE);
        button.setBorder(new EmptyBorder(0, 5, 15, 10));
        button.setFocusPainted(false);
        button.setContentAreaFilled(false);
        button.setCursor(new Cursor(Cursor.HAND_CURSOR));

        button.addActionListener(listener);
        button.setOpaque(false);
    }

    public void setLabelText(String text)
    {
        label.setText(text);
    }

    public boolean isPause()
    {
        return pause;
    }
}
