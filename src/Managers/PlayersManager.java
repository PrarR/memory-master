package Managers;

import Panels.PlayersPanel;

public class PlayersManager
{
    public enum Turn
    {
        FirstPlayer,
        SecondPlayer
    }

    private final SettingsManager settingsManager;

    private PlayersPanel playersPanel;
    private Turn playerTurn;
    private int firstPlayerPoints;
    private int secondPlayerPoints;

    public PlayersManager(SettingsManager settingsManager)
    {
        this.settingsManager = settingsManager;

        playerTurn = setInitiallyDrawnTurn();
        firstPlayerPoints = 0;
        secondPlayerPoints = 0;
    }

    private Turn setInitiallyDrawnTurn()
    {
        int randInt = settingsManager.getRandom().nextInt(2);

        if (randInt == 0)
            return Turn.FirstPlayer;
        else
            return Turn.SecondPlayer;
    }

    public void addPlayersPoints()
    {
        if (playerTurn == Turn.FirstPlayer)
        {
            playersPanel.updatePlayerPointsLabel(playerTurn, ++firstPlayerPoints);
            return;
        }

        if (playerTurn == Turn.SecondPlayer)
            playersPanel.updatePlayerPointsLabel(playerTurn, ++secondPlayerPoints);
    }

    public void setNextTurn()
    {
        if (playerTurn == Turn.FirstPlayer)
        {
            playerTurn = Turn.SecondPlayer;
            playersPanel.updateCurrentTurnLabel(playerTurn, settingsManager.getSecondPlayer());
            return;
        }

        if (playerTurn == Turn.SecondPlayer)
        {
            playerTurn = Turn.FirstPlayer;
            playersPanel.updateCurrentTurnLabel(playerTurn, settingsManager.getSecondPlayer());
        }
    }

    public void setPlayersPanel(PlayersPanel playersPanel)
    {
        this.playersPanel = playersPanel;
    }

    public int getFirstPlayerPoints()
    {
        return firstPlayerPoints;
    }

    public int getSecondPlayerPoints()
    {
        return secondPlayerPoints;
    }

    public Turn getPlayerTurn()
    {
        return playerTurn;
    }
}
