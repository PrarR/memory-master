package Managers;

import java.util.Random;

public class SettingsManager
{
    public enum Difficulty
    {
        Easy(0, 4, 4, 60, 3, 6, 90),
        Medium(1, 4, 6, 70, 5, 8, 150),
        Hard(2, 5, 8, 75, 6, 12, 270),
        Ultima(3, 6, 10, 80, 8, 16, 450);

        private final int index;
        private final int numberOfRows;
        private final int numberOfColumns;
        private final int chanceForCorrectMatch;
        private final int numberOfRememberedMoves;
        private final int numberOfMovesToShuffle;
        private final int time;

        Difficulty(int index, int numberOfRows, int numberOfColumns,int chanceForCorrectMatch,
                   int numberOfRememberedMoves, int numberOfMovesToShuffle, int time)
        {
            this.index = index;
            this.numberOfRows = numberOfRows;
            this.numberOfColumns = numberOfColumns;
            this.chanceForCorrectMatch = chanceForCorrectMatch;
            this.numberOfRememberedMoves = numberOfRememberedMoves;
            this.numberOfMovesToShuffle = numberOfMovesToShuffle;
            this.time = time;
        }

        public int getIndex()
        {
            return index;
        }

        public int getNumberOfRows()
        {
            return numberOfRows;
        }

        public int getNumberOfColumns()
        {
            return numberOfColumns;
        }

        public int getNumberOfCards()
        {
            return numberOfRows * numberOfColumns;
        }

        public int getChanceForCorrectMatch()
        {
            return chanceForCorrectMatch;
        }

        public int getNumberOfRememberedMoves()
        {
            return numberOfRememberedMoves;
        }

        public int getNumberOfMovesToShuffle()
        {
            return numberOfMovesToShuffle;
        }

        public int getTime()
        {
            return time;
        }
    }

    public enum PlayerMode
    {
        Singleplayer,
        Multiplayer
    }

    public enum SecondPlayer
    {
        Friend,
        AI
    }

    public enum GameMode
    {
        Standard,
        Shuffle,
        Time
    }

    private Difficulty difficulty;
    private PlayerMode playerMode;
    private SecondPlayer secondPlayer;
    private GameMode gameMode;
    private final Random random;

    public SettingsManager()
    {
        difficulty = Difficulty.Easy;
        playerMode = PlayerMode.Singleplayer;
        secondPlayer = SecondPlayer.Friend;
        gameMode = GameMode.Standard;
        random = new Random();
    }

    public void setDifficulty(Difficulty difficulty)
    {
        this.difficulty = difficulty;
    }

    public Difficulty getDifficulty()
    {
        return difficulty;
    }

    public void setPlayerMode(PlayerMode playerMode)
    {
        this.playerMode = playerMode;
    }

    public PlayerMode getPlayerMode()
    {
        return playerMode;
    }

    public void setSecondPlayer(SecondPlayer secondPlayer)
    {
        this.secondPlayer = secondPlayer;
    }

    public SecondPlayer getSecondPlayer()
    {
        return secondPlayer;
    }

    public void setGameMode(GameMode gameMode)
    {
        this.gameMode = gameMode;
    }

    public GameMode getGameMode()
    {
        return gameMode;
    }

    public Random getRandom()
    {
        return random;
    }
}
